#!/usr/bin/python
import sys
import os
import subprocess
from scripts_hgt.get_hgt import get_hgt
from scripts_hgt.hgt_to_osm import hgt_to_osm
from get_contours import get_contours
import os, shutil, fnmatch
import subprocess
import time
import pathlib
from multiprocessing import Pool

def task(country):
    country_name=country[0].replace("#",'')
    id=country[1]
    style=country[2]
    url=country[3]
    hasFiles=False
    country_dir =  "carte_"+country_name.replace(' ','_').lower()
    for file in pathlib.Path(country_dir).glob("*.img"):
        if(str(file).split("/")[1].startswith("55")):
            hasFiles=True
    if(hasFiles==False):
        #Get contours
        print("Download hgt "+country_name+ " "+id)
        country_name_lower_case = country_name.lower().replace(" ", "_")

        os.makedirs("dem/"+country_name_lower_case, exist_ok=True)

        os.makedirs("carte_"+country_name_lower_case, exist_ok=True)

        #Get poly file
        url_poly= url.replace("-latest.osm.pbf",".poly")

        #Get hgt
        get_hgt(country_name_lower_case, url_poly)
        #Download
        if ( os.path.isfile("dem/"+country_name_lower_case+"/hgt_urls.txt")): 
            subprocess.run(["bash", "scripts_hgt/download_hgt.sh","dem/"+country_name_lower_case+"/hgt_urls.txt"])


if __name__ == '__main__':
    country_list=[]

    #File country
    file_in = open("country.txt", "rt")

    lines = file_in.readlines()
    for line in lines:
        result = line.split(";")
        country_list.append([result[0],result[1],result[2],result[3]])

    file_in.close()

    with Pool() as pool:
        pool.map(task, country_list)
