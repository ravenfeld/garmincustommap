#!/bin/bash

for d in */ ; do
    if [[ "$d" == "carte_"* ]]; then
        cd $d
        rm *.pbf
        rm *.poly
        cd ..
    fi
done